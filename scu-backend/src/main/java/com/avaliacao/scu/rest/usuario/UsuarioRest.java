package com.avaliacao.scu.rest.usuario;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avaliacao.scu.domain.usuario.Usuario;
import com.avaliacao.scu.dto.usuario.UsuarioDTO;
import com.avaliacao.scu.service.usuario.UsuarioService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("usuario")
@RequiredArgsConstructor
@CrossOrigin
public class UsuarioRest {
	
	@Autowired
	UsuarioService service;
	
	@PostMapping("salvar")
    public ResponseEntity<?> salvar(@RequestBody Usuario usuario) {
		service.salvarUsuario(usuario);
        return ResponseEntity.ok(null);
    }

	@GetMapping("findAll")
    public List<UsuarioDTO> findAll() {
        return service.findAll();
    }
}
