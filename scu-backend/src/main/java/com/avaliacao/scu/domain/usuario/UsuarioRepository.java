package com.avaliacao.scu.domain.usuario;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.avaliacao.scu.dto.usuario.UsuarioDTO;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	@Query(" SELECT new com.avaliacao.scu.dto.usuario.UsuarioDTO(u.nome,u.login) from Usuario u")
	List<UsuarioDTO> findAllUsuarios();
	
	List<Usuario> findByLogin(String login);
	

}
