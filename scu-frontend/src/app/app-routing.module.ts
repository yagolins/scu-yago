import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioModule } from './main/usuario/usuario.module';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => UsuarioModule,
    data: {
      breadcrumb: 'Usuário'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
