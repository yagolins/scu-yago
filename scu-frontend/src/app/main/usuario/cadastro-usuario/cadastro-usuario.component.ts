import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogConfig, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UsuarioService } from '../usuario.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DialogService } from '../modal/dialog.service';

@Component({
  selector: 'app-cadastro-usuario',
  templateUrl: './cadastro-usuario.component.html',
  styleUrls: ['./cadastro-usuario.component.scss']
})
export class CadastroUsuarioComponent implements OnInit {

  formUsuario: FormGroup;

  constructor(private fb: FormBuilder,
    public usuarioService: UsuarioService,
    private modal: DialogService,
    private matDialog: MatDialog,
    private dialogRef: MatDialogRef<CadastroUsuarioComponent>) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.formUsuario = this.fb.group({
      nome: new FormControl('', [Validators.required]),
      login: new FormControl('', [Validators.required]),
      senha: new FormControl('', [Validators.required,Validators.minLength(5)]),
    });

  }

  fecharModal() {
    this.dialogRef.close();
  }

  static getConfDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.height = '40em';
    dialogConfig.width = '30em';
    return dialogConfig;
  }

  cadastrar() {
    if(this.formUsuario.get('senha').value.length <5){
      this.modal.alert('A senha precisa ter pelo menos 5 caracteres');
      return;
    }
    if(this.formUsuario.invalid){
      this.modal.alert('Existem campos obrigatórios a serem preenchidos!');
      return;
    }
    let usuario = {
      nome: this.formUsuario.get('nome').value,
      login: this.formUsuario.get('login').value,
      senha: btoa(this.formUsuario.get('senha').value)
    }

    this.usuarioService.persistir(usuario,
      success => {
        this.modal.alert('Usuário cadastrado com sucesso!');
        this.fecharModal();
      },
      error => {
         const msg = error.error.message;
         this.modal.alert(msg, 'ERRO');
         return error;
      });
  }
}




