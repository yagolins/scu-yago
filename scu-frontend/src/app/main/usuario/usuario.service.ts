import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  urlBase = 'http://localhost:8080/usuario';
  headers: HttpHeaders;
  clientId: string;

  constructor(private _httpClient: HttpClient) { }

  findAll(callbackSuccess: any, callbackError: any) {
    const option = {
      headers: this.headers
    };
    this._httpClient.get(
      this.urlBase + '/findAll', option)
      .subscribe(
        (response: any) => {
          callbackSuccess(response);
        },
        error => {
          callbackError(error);
        }
      );
  }

  persistir(dto: any,callbackSuccess: any, callbackError: any) {
    const option = {
      headers: this.headers
    };
    this._httpClient.post(
      this.urlBase + '/salvar',dto,  option)
      .subscribe(
        (response: any) => {
          callbackSuccess(response);
        },
        error => {
          callbackError(error);
        }
      );
  }
}
