import { Component, OnInit, ViewChild } from '@angular/core';
import { CadastroUsuarioComponent } from '../cadastro-usuario/cadastro-usuario.component';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.scss']
})
export class ListaUsuariosComponent implements OnInit {

  displayedColumns = ['nome', 'login'];

  usuarios: any[] = [];

  constructor(
    private matDialog: MatDialog,
    public usuarioService: UsuarioService ) { }

  ngOnInit() {
    this.findAll();
  }

  adicionar() {
    const modalImagem = this.matDialog.open(CadastroUsuarioComponent, CadastroUsuarioComponent.getConfDialog());
     modalImagem.afterClosed().subscribe((response: any) => {
       console.log(response);
      this.findAll();
    },
      error => {

      });
  }

  findAll(){
    this.usuarioService.findAll(
      success => {
        this.usuarios = success;
      },
      error => {
        /* const msg = !!error.error.message ? error.error.message : MensagemUtil.MSG_ERRO;
        this.modal.alert(msg, error.status === MensagemUtil.STATUS.CONFLICT ? MensagemUtil.MSG.AVISO : MensagemUtil.MSG.ERRO);
        return error; */
      });
  }
}
